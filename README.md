# Mail Server Exim/Dovecot + Postfixadmin + RoundCube + SSL + MySQL Backend

**Подготовка серверов**
----
Данная установка подразумевает что будет запущено два VPS.

На одной машине установлена Ansible, на другой будет установлен Почтовика.

После установки Почтовика, машину с Ansible удаляем.
Все инструкции ниже с видео мануалом.
Перед установкой нужно направить домен на хостинг где будет установлена Почтовик.


**Настройки домена в  DNS**
--

Направляем три домена:

`mail.domain.com` - для почтовых клиентов

`webmail.domain.com` - для RoundCube

`emlmgmt.domain.com` - для админки

После окончания установки будет установлен SSL сертификат Let's Encrypt.
Установка включает в себя установку всех необходимых серверных компонентов чтобы работать уже в рабочей версии для продакшена.


**Установка** 
--
Установка Ansible

https://journal.flectrahq.ru/Ustanovka-Ansible-Ubuntu


Подключить уделенную машину, где будет установлена Почтовик

https://journal.flectrahq.ru/Podklyuchenie-udalennyh-hostov-SSH-Ansible

В файле host добавляем запись
your-ip - ваш IP почтовика
domain.com - главный домен почтовика, даже если вы используете поддомен для домена отправителя. Этот домен нужен для правильной настройки в конфигах почтовика.

[mailer]
your-ip ansible_ssh_user=root IP=your-ip domain=domain.com

Должно выглядеть примерно так:

[mailer]
0.0.0.0 ansible_ssh_user=root IP=0.0.0.0 domain=domain.com

После установки, заходим в каталог /root почтовика, там найдете файл
readme

Действуйте по инструкции там указанной

Там все пароли и явки.


**Записи DNS**
--
**Добавим DKIM запись** 

Это запись для главного домена, с которого будете отправлять письма
(domain.com - ваш главный домен)
main._domainkey.domain.com
v=DKIM1; k=rsa; p=код берете из файла /etc/exim4/dkim/domain.com.pub


Если отправляете от другого поддомена то, по следующей инструкции делаете на новый поддомен DKIM и добавляете соответствующую запись в DNS

`cd /etc/exim4/dkim`

`openssl genrsa -out plus.domain.com.key 1024`

`openssl rsa -in plus.domain.com.key -pubout > plus.domain.com.pub`

`chown mail:mail plus.domain.com.key`

`chmod 640 plus.domain.com.key`

Выглядеть должно так:

`main._domainkey.plus.domain.com v=DKIM1; k=rsa; p=код берете из файла /etc/exim4/dkim/plus.domain.com.pub`

Взято из инструкции:
https://tradenark.com.ua/debian/configure-dkim-for-exim-in-debian/

**Добавляем  _DMARC запись**

`_dmarc.domain.com  TXT v=DMARC1; p=reject; aspf=s; adkim=s`

**Добавляем SPF запись**

`TXT v=spf1 a mx -all`

**Добавляем MX запись**

`MX 10 mail.domain.com.`

`MX 20 mail.domain.com.`

**Добавим PTR запись**

Просим хостера добавить запись:

1. Ваш IP почтовика
2. mail.domain.com
3. `IP в обратном порядке числа.in-addr.arpa. IN PTR mail.domain.com`

Как пример домен 1.2.3.4 записываем его так

`4.3.2.1.in-addr.arpa. IN PTR mail.domain.com`

Тестировать рекомендую тут:

https://www.mail-tester.com/

Советы по настройке тут (если что, хотя все должно работать):

https://mxtoolbox.com/


Хостинг для почтовика можно использовать [Beget](https://beget.com/p786588)